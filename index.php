<?php
    // Cogemos el dato de la URL
    $number = $_POST['number'] ?? 'No hay ningún número asignado';

    // "Protección de ataques XSS"
    $number = htmlspecialchars($number, ENT_HTML401, ini_get("default_charset"), true);

    function printText($numberA)
    {
        $objetive = 42;
        // Decidir la página que se muestra al usuario según el $number
        if ($numberA == 'No hay ningún número asignado') { // Sin ningún valor 
            echo "<h3>$number por favor introduzca uno</h3>";
        } 
        else if (!is_numeric($numberA)) { // Siendo la entrada un String
            echo '<h3>Ingrese un número válido</h3>';
        } 
        else if ($numberA > $objetive) { // Siendo el número mayor que el que hay que encontrar
            echo '<h3>Te has pasado</h3>';
        } 
        else if ($numberA < $objetive) { // Siendo el número menor que el que hay que encontrar
            echo '<h3>Te has quedado corto</h3>';
        } 
        else if ($numberA == $objetive) { // Siendo el número el que hay que encontrar
            echo '<h3>¡HAS ENCONTRADO EL NÚMERO!</h3>';
        }
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iago Senín</title>
</head>
<body>
    <h1>Adivina el número: el juego</h1>
    <?php printText($number) ?>
    <form action="" method="POST">
        <input id="number" name="number" type="text" value="<?= $number ?>">
        <input type="submit" value="Probar">
    </form>
</body>
</html>